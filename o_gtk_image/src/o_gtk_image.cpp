#include "common.h"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
//#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include "blc_core.h"
#include "blc_program.h"
float min, max;

const char *channel_name, *fullscreen_option, *keyboard_mode;
GtkWidget *window;
GdkDisplay *main_display;
GdkDevice *pointer_device;
int interactive_mode=0;
GtkApplication *app;
blc_channel mouse_channel;

blc_channel input;

char const *input_name;

int input_terminal;
int mode;

char const *format_string;
uint32_t format;

void ask_fullscreen(){
    gtk_window_fullscreen(GTK_WINDOW(window));
    
}

void ask_quit()
{
    g_application_quit(G_APPLICATION(app));
    blc_quit();
}

void on_key_press(GtkWidget	     *widget, GdkEventKey	     *event){
    
    char key;
    
    if (event->type == GDK_KEY_PRESS){
        key=event->keyval;
        fwrite(&key, 1, 1, stdout);
        fflush(stdout);
    }
}

void activate_cb(GApplication *app)
{
    GtkWidget *display=NULL;
    GtkWidget *grid;
    
    main_display = gdk_display_get_default ();
    GdkDeviceManager *device_manager = gdk_display_get_device_manager (main_display);
    pointer_device = gdk_device_manager_get_client_pointer (device_manager);
    
    window=gtk_application_window_new(GTK_APPLICATION(app));
    gtk_window_set_title(GTK_WINDOW(window), input.name);
    
    grid=gtk_grid_new();
    
    //  for(i=0; input_names[i]; i++){ This is for displaying multiple images
    //  input=new blc_channel(/*input_names[i]*/ input_name, BLC_CHANNEL_READ);
    display=create_image_display(&input);
    if (display==NULL) EXIT_ON_CHANNEL_ERROR(&input, "Format not managed.");
    gtk_widget_set_hexpand(display, 1);
    gtk_widget_set_vexpand(display, 1);
    gtk_container_add(GTK_CONTAINER(grid), display);
    //  }
    gtk_container_add(GTK_CONTAINER(window), grid);
    gtk_widget_show_all(window);
    if (keyboard_mode) g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK (on_key_press), NULL);
    
}

/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char *argv[])
{
    int status=0;
    char const *g_debug, *mouse_name, *min_str, *max_str;
    blc_channel channel_info;
    
    blc_program_set_description("Display the content of the blc_channel depending on its type on format");
    blc_program_add_option(&keyboard_mode, 'k', "keyboard", NULL, "Send keyboard input to stdout", NULL);
    blc_program_add_option(&min_str, 'm', "min", "FL32", "minimal value", NULL);
    blc_program_add_option(&max_str, 'M', "max", "FL32", "maximal value", NULL);
    //  blc_program_add_option(&mouse_name, 'm', "mouse", "blc_channel-out", "return the mouse coordinates  and status in the channel", NULL);
    blc_program_add_option(&fullscreen_option, 'F', "fullscreen", NULL, "Set the window in fullscreen", NULL);
    blc_program_add_option(&g_debug, ' ', "g-fatal-warnings", NULL, "Debug gtk.", NULL);
    // This function is not yer operaiotnal  blc_program_add_multiple_parameters(&input_names, "blc_channel", 1, "channel name you want to display");
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "channel name you want to display", NULL);
    
    blc_program_init(&argc, &argv, ask_quit);
    
    input.open(input_name, mode);
    
    min=0;
    if (input.type=='UIN8') max=256;
    if (input.type=='INT8'){
        min=-128;
        max=128;
    }
    
    
    if (input.type=='FL32' && input.format=='Y800'){
        if (min_str) SSCANF(1, min_str, "%f", &min);
        else min=0;
        if (max_str) SSCANF(1, max_str, "%f", &max);
        else max=1;
    }else if (min_str || max_str) EXIT_ON_ARRAY_ERROR(&input, "Min (-m) and max (-M) have only effect for type FL32 format Y800");
    
    gtk_disable_setlocale();
    gtk_init(&argc, &argv);
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
    status = g_application_run(G_APPLICATION(app), 0, NULL);
    g_object_unref(app);
    
    return EXIT_SUCCESS;
}

