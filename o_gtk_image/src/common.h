#ifndef COMMON_H
#define COMMON_H

#include <gtk/gtk.h>
#include <blc_channel.h>


extern float min, max;

extern blc_channel *channel;
extern GtkWidget *legend;

extern u_int32_t false_colors[256];
extern uint32_t gray_colors[256];
extern uint32_t u_colors[256];
extern uint32_t v_colors[256];
extern uint32_t r_colors[256];
extern uint32_t g_colors[256];
extern uint32_t b_colors[256];
extern uint32_t *color_map;

extern GtkWidget *window, *paned;
extern GdkDevice *pointer_device;
extern blc_channel mouse_channel;

extern char const *fullscreen_option;



void histogram_cb(GtkToolButton *toolbutton, gpointer pointer_statusbar );
GtkWidget *create_image_display(blc_channel *channel);

#endif

